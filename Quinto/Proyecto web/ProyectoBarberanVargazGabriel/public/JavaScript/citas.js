window.addEventListener('load', function () {
    btnnuevo.addEventListener('click', function () {
        txtcedula.value = "";
        txtnombre.value = "";
        txtfecha.value = "";
        txthora.value = "";
        txtpsicologo.value = "";
        txttelefono1.value = "";
        txtestado.value="";

    })

    btnconsultar.addEventListener('click', function () {
        //usando un metodo GET de consulta

        // let url ='https://proyectoejemplo-11b82.firebaseio.com/Cursos.json'
        //https://complementario-1e224.firebaseio.com/CitasPsicologia/${elemento.innerHTML.trim()}.json
        let url = `https://complementario-1e224.firebaseio.com/CitasPsicologia.json`
        fetch(url).then(resultado => {
            return resultado.json();
        })
            .then(resultado2 => {
                let tablaHtml = "<table border=1>";
                tablaHtml+="<tr>"
                    tablaHtml+="<td><h3>Cedula Identidad</h3></td>"
                    tablaHtml+= "<td><h3>Nombre</h3></td>"
                    tablaHtml+= "<td><h3>Fecha</h3></td>"
                    tablaHtml+= "<td><h3>Hora</h3></td>"
                    tablaHtml+= "<td><h3>Psicologo</h3></td>"
                    tablaHtml+= "<td><h3>Telefono</h3></td>"
                    tablaHtml+= "<td><h3>Estado</h3></td>"
                    tablaHtml+="</tr>"
                for (let elemento in resultado2) {
                    tablaHtml += "<tr>"
                    tablaHtml += `<td> <button class='boton'> ${resultado2[elemento].Cedula} </button></td> 
                    <td>${resultado2[elemento].NombrePaciente}</td>
                      <td>${resultado2[elemento].Fecha}</td>
                      <td>${resultado2[elemento].Hora}</td>
                      <td>${resultado2[elemento].Psicologo}</td>
                      <td>${resultado2[elemento].Telefono}</td> 
                      <td>${resultado2[elemento].Estado}</td>`


                    tablaHtml += "</tr>"
                }

                tablaHtml += "</table>"
                listado.innerHTML = tablaHtml;
                document.querySelectorAll('.boton').forEach(elemento => {
                    elemento.addEventListener('click', function () {
                        let url2 = `https://complementario-1e224.firebaseio.com/CitasPsicologia/${elemento.innerHTML.trim()}.json`


                        console.log(url2)

                        fetch(url2).then(respuesta => { return respuesta.json() }).then(respuesta2 => {
                            txtcedula.value = respuesta2.Cedula;
                            txtnombre.value = respuesta2.NombrePaciente;
                            txtfecha.value = respuesta2.Fecha;
                            txthora.value = respuesta2.Hora;
                            txtpsicologo.value = respuesta2.Psicologo;
                            txttelefono1.value= respuesta2.Telefono;
                            txtestado.value= respuesta2.Estado;
                        })


                    })
                })

            }).catch(error => {
                console.log(error)

            })

        btnguardar.addEventListener('click', function () {
            //método POST
            //`https://complementario-1e224.firebaseio.com/CitasPsicologia/${txtcodigo.value}.json`
            let url = `https://complementario-1e224.firebaseio.com/CitasPsicologia/${txtcedula.value}.json`
            let cuerpo = {
                Cedula: txtcedula.value,
                NombrePaciente: txtnombre.value,
                Fecha: txtfecha.value,
                Hora: txthora.value,
                Psicologo: txtpsicologo.value,
                Telefono:txttelefono1.value,
                Estado:txtestado.value

            }

            fetch(url, {
                method: 'PUT',
                body: JSON.stringify(cuerpo),
                headers: {
                    'Content-Type': 'application/json'
                }

            }).then(resultado => {
                return resultado.json();
            })
                .then(resultado2 => {
                    alert("Registro hecho correctamente")
                    console.log(resultado2.name)
                })
                .catch(error => {
                    console.error('Hubo un error al ejecutarse', error)
                })


        })
        btneliminar.addEventListener('click', function () {
            let url = `https://complementario-1e224.firebaseio.com/CitasPsicologia/${txtcedula.value}.json`

            fetch(url, {
                method: 'DELETE'
            })
                .then(resultado => {
                    alert("Se ha eliminado el registro , presionar boton CONSULTAR para verificar")
                    return resultado.json()
                })
                .then(resultado2 => {
                    console.log(resultado2)
                })
                .catch(error => {
                    console.error('No se pudo eliminar el curso', error)
                })

        })


    })

})