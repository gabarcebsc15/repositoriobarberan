window.addEventListener('load', function () {
    btnnuevo1.addEventListener('click', function () {
        txtid.value = "";
        txtnombre1.value = "";
        txttelefono.value = "";
        txtestado1.value = "";

    })

    btnconsultar1.addEventListener('click', function () {
        //usando un metodo GET de consulta

        // let url ='https://proyectoejemplo-11b82.firebaseio.com/Cursos.json'
        //https://complementario-1e224.firebaseio.com/CitasPsicologia/${elemento.innerHTML.trim()}.json
        let url = `https://complementario-1e224.firebaseio.com/Psicologo.json`
        fetch(url).then(resultado => {
            return resultado.json();
        })
            .then(resultado2 => {
                let tablaHtml = "<table border=1>";

                    tablaHtml+="<tr>"
                    tablaHtml+="<td><h3>ID</h3></td>"
                    tablaHtml+= "<td><h3>Nombre</h3></td>"
                    tablaHtml+= "<td><h3>Telefono</h3></td>"
                    tablaHtml+= "<td><h3>Estado</h3></td>"
                    tablaHtml+="</tr>"
                for (let elemento in resultado2) {
                    tablaHtml += "<tr>"
                    tablaHtml += `<td><button class='boton'> ${resultado2[elemento].ID} </button></td> 
                    <td>${resultado2[elemento].Nombre}</td>
                    <td>${resultado2[elemento].Telefono}</td>
                    <td>${resultado2[elemento].Estado}</td>`


                     tablaHtml += "</tr>"
                }

                tablaHtml += "</table>"
                listado1.innerHTML = tablaHtml;
                document.querySelectorAll('.boton').forEach(elemento => {
                    elemento.addEventListener('click', function () {
                        let url2 = `https://complementario-1e224.firebaseio.com/Psicologo/${elemento.innerHTML.trim()}.json`


                        console.log(url2)

                        fetch(url2).then(respuesta => { return respuesta.json() }).then(respuesta2 => {
                            txtid.value = respuesta2.ID;
                            txtnombre1.value = respuesta2.Nombre;
                            txttelefono.value = respuesta2.Telefono;
                            txtestado1.value = respuesta2.Estado;
                        })


                    })
                })

            }).catch(error => {
                console.log(error)

            })

        btnguardar1.addEventListener('click', function () {
            //método POST
            //`https://complementario-1e224.firebaseio.com/CitasPsicologia/${txtcodigo.value}.json`
            let url = `https://complementario-1e224.firebaseio.com/Psicologo/${txtid.value}.json`
            let cuerpo = {
                ID: txtid.value,
                Nombre: txtnombre1.value,
                Telefono: txttelefono.value,
                Estado: txtestado1.value

            }

            fetch(url, {
                method: 'PUT',
                body: JSON.stringify(cuerpo),
                headers: {
                    'Content-Type': 'application/json'
                }

            }).then(resultado => {
                return resultado.json();
            })
                .then(resultado2 => {
                    alert("Registro hecho correctamente")
                    console.log(resultado2.name)
                })
                .catch(error => {
                    console.error('Hubo un error al ejecutarse', error)
                })


        })
        btneliminar1.addEventListener('click', function () {
            let url = `https://complementario-1e224.firebaseio.com/Psicologo/${txtid.value}.json`

            fetch(url, {
                method: 'DELETE'
            })
                .then(resultado => {
                    alert("Se ha eliminado el registro , presionar boton CONSULTAR para verificar")
                    return resultado.json()
                })
                .then(resultado2 => {
                    console.log(resultado2)
                })
                .catch(error => {
                    console.error('No se pudo eliminar el curso', error)
                })

        })


    })

})