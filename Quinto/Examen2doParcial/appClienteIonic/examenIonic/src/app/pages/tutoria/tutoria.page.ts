import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Alumno } from 'src/app/interfaces/alumno';
import { ITutoria } from 'src/app/interfaces/ITutoria';
import { Tutoria } from 'src/app/interfaces/tutoria';
import { TutoriaService } from 'src/app/services/tutoria.service';

@Component({
  selector: 'app-tutoria',
  templateUrl: './tutoria.page.html',
  styleUrls: ['./tutoria.page.scss'],
})
export class TutoriaPage implements OnInit, ITutoria {


  public registroTutoria: Tutoria =
    {
      ID: '',
      NombreEstudiante: '',
      IDEstudiante: '',
      NombreDocente: '',
      IDDocente: '',
      TemaTratado: '',
      UnidadSilabo: '',
      NivelTutoria: '',
      Detalle: '',
      Observacion: ''
    }
    public agregaralumno : Alumno ={Identificacion:'', Nombre:''}

  public registrosTutoria: Tutoria[] = []
  public agregaralumnos:Alumno[]=[]



  constructor(private cliente: TutoriaService, private toast: ToastController) { }

  consultaIndividual(identificacionx:string)
  {
      this.cliente.getAlumno(identificacionx).then(respuesta=>{
        this.agregaralumno= <Alumno>respuesta;
      })
      .catch(error=>{
        console.log(error)
      })

  }
 
  consultar2(): void {
    this.cliente.getAlumno().then(respuesta => {

      this.agregaralumnos = [];
      for (let elemento in respuesta) {
        this.agregaralumnos.push(respuesta[elemento]);
      }

    })
      .catch(err => {
        console.log(err)
      })
    
  }

  async mostrarMensaje(mensaje: string, duracion: number) {
    const mensajex = await this.toast.create({ message: mensaje, duration: duracion });
    mensajex.present();
  }


  grabar(): void {
    this.cliente.postTutoria(this.registroTutoria).then(respuesta => {
      this.mostrarMensaje('Registro exitoso', 2000)

    }).catch(error => {
      console.log('no se pudo almacenar el registro')
    })


  }
  nuevo(): void {
    this.registroTutoria.ID='';
    this.registroTutoria.IDEstudiante='';
    this.registroTutoria.NombreEstudiante='';
    this.registroTutoria.IDDocente='';
    this.registroTutoria.NombreDocente='';
    this.registroTutoria.TemaTratado='';
    this.registroTutoria.UnidadSilabo='';
    this.registroTutoria.NivelTutoria='';
    this.registroTutoria.Detalle
    this.registroTutoria.Observacion='';
  }
  consultar(): void {
    this.cliente.getTutoria().then(respuesta => {

      this.registrosTutoria = [];
      for (let elemento in respuesta) {
        this.registrosTutoria.push(respuesta[elemento]);
      }

    })
      .catch(err => {
        console.log(err)
      })
  }

  ngOnInit() {
  }

}
