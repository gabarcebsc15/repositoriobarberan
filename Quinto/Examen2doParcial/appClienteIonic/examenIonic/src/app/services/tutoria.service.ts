import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tutoria } from '../interfaces/tutoria';
import { Alumno } from '../interfaces/alumno';

@Injectable({
  providedIn: 'root'
})
export class TutoriaService {

  apiURL =`https://tutoriasfacci-a6368.firebaseio.com/Tutorias`
  apiURL2=`https://tutoriasfacci-a6368.firebaseio.com/Alumnos`

  constructor( private clienteTutoria: HttpClient ) { }

  public getTutoria(ID='')
  {
    if(ID=='')
    return this.clienteTutoria.get(`${this.apiURL}.json`).toPromise()
    return this.clienteTutoria.get(`${this.apiURL}/${ID}.json`).toPromise()

  }
  public getAlumno(Identificacion='')
  {
    if(Identificacion=='')
    return this.clienteTutoria.get(`${this.apiURL2}.json`).toPromise()
    return this.clienteTutoria.get(`${this.apiURL2}/${Identificacion}.json`).toPromise()

  }
  public postTutoria(tutoriax: Tutoria)
  {
    return this.clienteTutoria.put(`${this.apiURL}/${tutoriax.ID}.json`, 
    tutoriax, {headers: { 'Content-Type':'application/json' }} ).toPromise();
     
  }
}
