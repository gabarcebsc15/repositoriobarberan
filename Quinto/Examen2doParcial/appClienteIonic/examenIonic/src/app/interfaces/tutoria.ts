export class Tutoria
{
    ID: string
    NombreEstudiante:string
    IDEstudiante:string
    NombreDocente:string
    IDDocente:string
    TemaTratado:string
    UnidadSilabo:string
    NivelTutoria:string
    Detalle:string
    Observacion:string
}