window.addEventListener('load', function () {
    btnnuevo.addEventListener('click', function () {
        txtid.value = "";
        txtestudiante.value = "";
        txte.value = "";
        txtdocente.value = "";
        txtd.value = "";
        txttema.value = "";
        txtunidad.value = "";
        txtnivel.value = "";
        txtdetalles.value = "";
        txtobservacion.value = "";



    })

    btnconsultar.addEventListener('click', function () {
        //usando un metodo GET de consulta

        // let url ='https://proyectoejemplo-11b82.firebaseio.com/Cursos.json'
        //https://complementario-1e224.firebaseio.com/CitasPsicologia/${elemento.innerHTML.trim()}.json
        let url = `https://tutoriasfacci-a6368.firebaseio.com/Tutorias.json`
        fetch(url).then(resultado => {
            return resultado.json();
        })
            .then(resultado2 => {
                let tablaHtml = "<table border=1>";
                tablaHtml += "<tr>"
                tablaHtml += "<td><h3>ID</h3></td>"
                tablaHtml += "<td><h3>NombreEstudainte</h3></td>"
                tablaHtml += "<td><h3>C.I Est.</h3></td>"
                tablaHtml += "<td><h3>NombreDocente</h3></td>"
                tablaHtml += "<td><h3>C.I Doc.</h3></td>"
                tablaHtml += "<td><h3>Tema Tratado</h3></td>"
                tablaHtml += "<td><h3>Unidad Sílabo</h3></td>"
                tablaHtml += "<td><h3>Nivel Tutoria</h3></td>"
                tablaHtml += "<td><h3>Detalle</h3></td>"
                tablaHtml += "<td><h3>Observacion</h3></td>"
                tablaHtml += "</tr>"
                for (let elemento in resultado2) {
                    tablaHtml += "<tr>"
                    tablaHtml += `<td> <button class='boton'> ${resultado2[elemento].ID} </button></td> 
                    <td>${resultado2[elemento].NombreEstudiante}</td>
                      <td>${resultado2[elemento].IDEstudiante}</td>
                      <td>${resultado2[elemento].NombreDocente}</td>
                      <td>${resultado2[elemento].IDDocente}</td>
                      <td>${resultado2[elemento].TemaTratado}</td> 
                      <td>${resultado2[elemento].UnidadSilabo}</td>
                      <td>${resultado2[elemento].NivelTutoria}</td>
                      <td>${resultado2[elemento].Detalle}</td>
                      <td>${resultado2[elemento].Observacion}</td>`


                    tablaHtml += "</tr>"
                }

                tablaHtml += "</table>"
                listado.innerHTML = tablaHtml;
                document.querySelectorAll('.boton').forEach(elemento => {
                    elemento.addEventListener('click', function () {
                        let url2 = `https://tutoriasfacci-a6368.firebaseio.com/Tutorias/${elemento.innerHTML.trim()}.json`
                        //`https://tutoriasfacci-a6368.firebaseio.com/Tutorias/${elemento.innerHTML.trim()}.json`


                        console.log(url2)

                        fetch(url2).then(respuesta => { return respuesta.json() }).then(respuesta2 => {
                            txtid.value = respuesta2.ID;
                            txtestudiante.value = respuesta2.NombreEstudiante;
                            txte.value = respuesta2.IDEstudiante;
                            txtdocente.value = respuesta2.NombreDocente;
                            txtd.value = respuesta2.IDDocente;
                            txttema.value = respuesta2.TemaTratado;
                            txtunidad.value = respuesta2.UnidadSilabo;
                            txtnivel.value = respuesta2.NivelTutoria;
                            txtdetalles.value = respuesta2.Detalle;
                            txtobservacion.value = respuesta2.Observacion;
                        })


                    })
                })

            }).catch(error => {
                console.log(error)

            })

        btnguardar.addEventListener('click', function () {
            //método POST
            //`https://complementario-1e224.firebaseio.com/CitasPsicologia/${txtcodigo.value}.json`

            let url = `https://tutoriasfacci-a6368.firebaseio.com/Tutorias/${txtid.value}.json`
            let cuerpo = {
                ID: txtid.value,
                NombreEstudiante: txtestudiante.value,
                IDEstudiante: txte.value,
                NombreDocente: txtdocente.value,
                IDDocente: txtd.value,
                TemaTratado: txttema.value,
                UnidadSilabo: txtunidad.value,
                NivelTutoria: txtnivel.value,
                Detalle: txtdetalles.value,
                Observacion: txtobservacion.value


            }

            fetch(url, {
                method: 'PUT',
                body: JSON.stringify(cuerpo),
                headers: {
                    'Content-Type': 'application/json'
                }

            }).then(resultado => {
                return resultado.json();
            })
                .then(resultado2 => {
                    alert("Registro hecho correctamente")
                    console.log(resultado2.name)
                })
                .catch(error => {
                    console.error('Hubo un error al ejecutarse', error)
                })


        })
        btneliminar.addEventListener('click', function () {
            let url = `https://tutoriasfacci-a6368.firebaseio.com/Tutorias/${txtid.value}.json`

            fetch(url, {
                method: 'DELETE'
            })
                .then(resultado => {
                    alert("Se ha eliminado el registro , presionar boton CONSULTAR para verificar")
                    
                    return resultado.json()
                })
                .then(resultado2 => {
                    console.log(resultado2)
                })
                .catch(error => {
                    console.error('No se pudo eliminar el curso', error)
                })

        })


    })

})