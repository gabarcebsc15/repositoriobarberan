import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Psicologia } from '../interfaces/psicologia';
import { Psicologos } from '../interfaces/psicologos';
//import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PsicologiaService {

apiURL=`https://complementario-1e224.firebaseio.com/CitasPsicologia`
apiURL1=`https://complementario-1e224.firebaseio.com/Psicologo`
  
  constructor(private clientePsicologia: HttpClient) { }
  //constructor(private datosPsicologo : HttpClient){ }

  public getCitas()
  {
    return this.clientePsicologia.get(`${this.apiURL}.json`).toPromise()

  }
  public postCitas( citasx: Psicologia)
  {
    return this.clientePsicologia.put(`${this.apiURL}/${citasx.Cedula}.json`
    , citasx , {headers: { 'Content-Type':'application/json' }} ).toPromise();
    
  }
  public getPsicologo(ID='')
  {
    if(ID=='')
    return this.clientePsicologia.get(`${this.apiURL1}.json`).toPromise()
    return this.clientePsicologia.get(`${this.apiURL1}/${ID}.json`).toPromise()

  }
  

}
