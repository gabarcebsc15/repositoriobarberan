import { Component, OnInit } from '@angular/core';
import { Psicologia } from 'src/app/interfaces/psicologia';
import { ICrud } from 'src/app/interfaces/iCrud';
import { PsicologiaService } from 'src/app/services/psicologia.service';
import { ToastController } from '@ionic/angular';
import { Psicologos } from 'src/app/interfaces/psicologos';

@Component({
  selector: 'app-psicologia',
  templateUrl: './psicologia.page.html',
  styleUrls: ['./psicologia.page.scss'],
})
export class PsicologiaPage implements OnInit, ICrud {

  public psicologiaCita: Psicologia =
    { Cedula: '', NombrePaciente: '', Psicologo: '', Telefono: 0, Hora: '', Fecha: '', Estado: 'En espera..' };

  public psicologo: Psicologos =
    { ID: 0, Nombre: '', Telefono: '', Estado: '' }

  //:{cedula:string, nombrepaciente: string, fecha: string, hora: string,psicologo: string, cupo: number}=
  //{ cedula:'1213', nombrepaciente:'Gabriel',fecha:'vacio',hora: '',psicologo:'', cupo:0};
  public psicologiaCitas: Psicologia[] = [];
  public psicologos: Psicologos[] = [];

  constructor(private cliente: PsicologiaService, private toast: ToastController) {
  }
  consultar1(): void {
    this.cliente.getPsicologo().then(respuesta => {

      this.psicologos = [];
      for (let elemento in respuesta) {
        this.psicologos.push(respuesta[elemento]);
      }

    })
      .catch(err => {
        console.log(err)
      })

  }

  async mostrarMensaje(mensaje: string, duracion: number) {
    const mensajex = await this.toast.create({ message: mensaje, duration: duracion });
    mensajex.present();
  }


  consultar(): void {

    this.cliente.getCitas().then(respuesta => {

      this.psicologiaCitas = [];
      for (let elemento in respuesta) {
        this.psicologiaCitas.push(respuesta[elemento]);
      }

    })
      .catch(err => {
        console.log(err)
      })
  }
  seleccionarPsicologo(idx:string)
  {
    this.cliente.getPsicologo(idx).then(respuesta=>{
      this.psicologo= <Psicologos>respuesta;
    })
    .catch(error=>{
      console.log(error)
    })
  }
  guardar(): void {

    this.cliente.postCitas(this.psicologiaCita).then(respuesta => {
      this.mostrarMensaje('Su solicitud fué enviada correctamente', 2000)

    }).catch(error => {
      console.log('no se pudo almacenar la cita')
    })


  }

  ngOnInit() {
  }
  nuevo() {
    this.psicologiaCita.Cedula = '';
    this.psicologiaCita.NombrePaciente = '';
    this.psicologiaCita.Fecha = '';
    this.psicologiaCita.Hora = '';
    this.psicologiaCita.Psicologo = '';
    this.psicologiaCita.Telefono = 0;
  }

}
