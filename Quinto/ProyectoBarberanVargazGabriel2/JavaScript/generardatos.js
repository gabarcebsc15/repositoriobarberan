window.addEventListener('load', function () {
    btnnuevo.addEventListener('click', function () {
        txtcodigo.value = "";
        txtnombre.value = "";
        txtautor.value = "";
        txtaño.value = "";
        txteditorial.value = "";

    })

    btnconsultar.addEventListener('click', function () {
        //usando un metodo GET de consulta

        // let url ='https://proyectoejemplo-11b82.firebaseio.com/Cursos.json'
        let url = `https://complementario-1e224.firebaseio.com/Libros.json`
        fetch(url).then(resultado => {
            return resultado.json();
        })
            .then(resultado2 => {
                let tablaHtml = "<table border=1>";
                for (let elemento in resultado2) {
                    tablaHtml += "<tr>"
                    tablaHtml += `<td> <button class='boton'> ${resultado2[elemento].Codigo} </button></td> 
                    <td>${resultado2[elemento].Nombre}</td>
                      <td>${resultado2[elemento].Autor}</td>
                      <td>${resultado2[elemento].Año}</td>
                      <td>${resultado2[elemento].Editorial}</td>`


                    tablaHtml += "</tr>"
                }

                tablaHtml += "</table>"
                listado.innerHTML = tablaHtml;
                document.querySelectorAll('.boton').forEach(elemento => {
                    elemento.addEventListener('click', function () {
                        let url2 = `https://complementario-1e224.firebaseio.com/Libros/${elemento.innerHTML.trim()}.json`

                        console.log(url2)

                        fetch(url2).then(respuesta => { return respuesta.json() }).then(respuesta2 => {
                            txtcodigo.value = respuesta2.Codigo;
                            txtnombre.value = respuesta2.Nombre;
                            txtautor.value = respuesta2.Autor;
                            txtaño.value = respuesta2.Año;
                            txteditorial.value = respuesta2.Editorial;
                        })


                    })
                })

            }).catch(error => {
                console.log(error)

            })

        btnguardar.addEventListener('click', function () {
            //método POST
            let url = `https://complementario-1e224.firebaseio.com/Libros/${txtcodigo.value}.json`
            let cuerpo = {
                Codigo: txtcodigo.value,
                Nombre: txtnombre.value,
                Autor: txtautor.value,
                Año: txtaño.value,
                Editorial: txteditorial.value
            }

            fetch(url, {
                method: 'PUT',
                body: JSON.stringify(cuerpo),
                headers: {
                    'Content-Type': 'application/json'
                }

            }).then(resultado => {
                return resultado.json();
            })
                .then(resultado2 => {
                    console.log(resultado2.name)
                })
                .catch(error => {
                    console.error('Hubo un error al ejecutarse', error)
                })


        })
        btneliminar.addEventListener('click', function () {
            let url = `https://complementario-1e224.firebaseio.com/Libros/${txtcodigo.value}.json`

            fetch(url, {
                method: 'DELETE'
            })
                .then(resultado => {
                    return resultado.json()
                })
                .then(resultado2 => {
                    console.log(resultado2)
                })
                .catch(error => {
                    console.error('No se pudo eliminar el curso', error)
                })

        })


    })

})