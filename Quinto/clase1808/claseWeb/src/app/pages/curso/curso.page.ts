import { Component, OnInit } from '@angular/core';
import { Curso } from 'src/app/interfaces/curso';
import { ICrud } from 'src/app/interfaces/icrud';

@Component({
  selector: 'app-curso',
  templateUrl: './curso.page.html',
  styleUrls: ['./curso.page.scss'],
})
export class CursoPage implements OnInit,ICrud {

  // de tipo TYPE, es como una clase pero abreviada
  //VARIABLE COMPUESTA
 public cursoAuxiliar: Curso = {codigo:'000',descripcion:'Vacio', cupo:0};


 public cursosAuxiliar: Curso[]=
 [
  {codigo:'001',descripcion:'Primero A', cupo:25},
  {codigo:'002',descripcion:'Primero B', cupo:30},
  {codigo:'003',descripcion:'Primero C', cupo:40},
  {codigo:'004',descripcion:'Quinto A', cupo:50},
  {codigo:'005',descripcion:'Qionto B', cupo:50},
  {codigo:'006',descripcion:'Quinto D', cupo:50}
 ];
 //:{codigo: string , descripcion: string, cupo: number}
 //={codigo:'000',descripcion:'Vacio', cupo:0};

 //public cursosAuxiliar
 //:{codigo: string , descripcion: string, cupo: number}[];


 //codigo
 //descripcion
 //cupo

  constructor() { }
  grabar(): void {
    throw new Error("Method not implemented.");
  }
  consultar(): void {
    throw new Error("Method not implemented.");
  }
  eliminar(): void {
    throw new Error("Method not implemented.");
  }

  ngOnInit() {

  }
  nuevo()
  {
    //this.cursoAuxiliar.codigo='';
    this.cursoAuxiliar={descripcion:'',cupo:0, codigo:''};
  }

}
