export interface ICrud
{
    nuevo():void
    consultar():void
    eliminar():void
    guardar():void
}