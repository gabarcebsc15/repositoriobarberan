import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PsicologiaPage } from './psicologia.page';

describe('PsicologiaPage', () => {
  let component: PsicologiaPage;
  let fixture: ComponentFixture<PsicologiaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PsicologiaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PsicologiaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
