import { Component, OnInit } from '@angular/core';
import { ICrud } from 'src/app/interfaces/ICrud';
import { Psicologia } from 'src/app/interfaces/psicologos';
import { ToastController } from '@ionic/angular';
import { PsicologiaService } from 'src/app/services/psicologia.service';

@Component({
  selector: 'app-psicologia',
  templateUrl: './psicologia.page.html',
  styleUrls: ['./psicologia.page.scss'],
})
export class PsicologiaPage implements OnInit, ICrud {

  public psicologiaCita : Psicologia=  
   { Cedula: '', NombrePaciente: '', Psicologo: '', Telefono: 0, Hora: '', Fecha: '', Estado: 'En espera..' };
  
  public psicologiaCitas: Psicologia[] = [];

  constructor(private cliente: PsicologiaService, private toast: ToastController) { }


  async mostrarMensaje(mensaje: string, duracion: number) {
    const mensajex = await this.toast.create({ message: mensaje, duration: duracion });
    mensajex.present();
  }

  nuevo(): void {
    this.psicologiaCita.Cedula = '';
    this.psicologiaCita.NombrePaciente = '';
    this.psicologiaCita.Fecha = '';
    this.psicologiaCita.Hora = '';
    this.psicologiaCita.Psicologo = '';
    this.psicologiaCita.Telefono = 0;  }


  consultar(): void {
    this.cliente.getCitas().then(respuesta => {

      this.psicologiaCitas = [];
      for (let elemento in respuesta) {
        this.psicologiaCitas.push(respuesta[elemento]);
      }

    })
      .catch(err => {
        console.log(err)
      })
  }


  eliminar(): void {
    throw new Error("Method not implemented.");
  }


  guardar(): void {
    this.cliente.postCitas(this.psicologiaCita).then(respuesta => {
      this.mostrarMensaje('Su solicitud fué enviada correctamente', 2000)

    }).catch(error => {
      console.log('no se pudo almacenar la cita')
    })
  }



  ngOnInit() {
  }

}
