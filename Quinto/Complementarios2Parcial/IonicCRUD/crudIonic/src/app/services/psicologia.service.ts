import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Psicologia } from '../interfaces/psicologos';

@Injectable({
  providedIn: 'root'
})
export class PsicologiaService {

  apiURL=`https://psicologiacrud.firebaseio.com/CitasPsicologia`

  constructor( private clientePsicologia: HttpClient) { }
  public getCitas()
  {
    return this.clientePsicologia.get(`${this.apiURL}.json`).toPromise()

  }
  public postCitas( citasx: Psicologia)
  {
    return this.clientePsicologia.put(`${this.apiURL}/${citasx.Cedula}.json`
    , citasx , {headers: { 'Content-Type':'application/json' }} ).toPromise();
    
  }
  /*public getPsicologo(ID='')
  {
    if(ID=='')
    return this.clientePsicologia.get(`${this.apiURL1}.json`).toPromise()
    return this.clientePsicologia.get(`${this.apiURL1}/${ID}.json`).toPromise()

  }*/
  public deleteCitas()
  {

  }
  

}

