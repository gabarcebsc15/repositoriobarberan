window.addEventListener('load',function(){
    btnnuevo.addEventListener('click',function(){
        txtcodigo.value="";
        txtdescripcion.value="";
    })

    btnconsultar.addEventListener('click',function(){
        //usando un metodo GET de consulta

       // let url ='https://proyectoejemplo-11b82.firebaseio.com/Cursos.json'
        let url = `https://proyectoejemplo-11b82.firebaseio.com/Cursos.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            let tablaHtml= "<table border=1>";
            for ( let elemento in resultado2 )
            {
                tablaHtml+="<tr>"

                tablaHtml+=`<td> <button class='boton'> ${resultado2[elemento].Codigo } </button></td><td>${resultado2[elemento].Descripcion}</td>`

                tablaHtml+="</tr>"
            }

            tablaHtml+="</table>"

            divconsulta.innerHTML= tablaHtml;
            document.querySelectorAll('.boton').forEach(elemento=>{
                elemento.addEventListener('click',function(){
                    let url2 =`https://proyectoejemplo-11b82.firebaseio.com/Cursos/${elemento.innerHTML.trim()}.json`
                    console.log(url2)

                    fetch(url2).then(respuesta=>{return respuesta.json()}).then(respuesta2=>{
                        txtcodigo.value=respuesta2.Codigo;
                        txtdescripcion.value= respuesta2.Descripcion;
                    } )

                    
                })
            })

        })
        .catch(error=>{
            console.log(error)
        })

        
    })
    btngrabar.addEventListener('click',function(){
        //método POST
        let url =`https://proyectoejemplo-11b82.firebaseio.com/Cursos.json`
        let cuerpo = { Codigo: txtcodigo.value ,Descripcion: txtdescripcion.value }

        fetch(url , {
            method:'POST',
            body:  JSON.stringify(cuerpo) ,
            headers:{
                'Content-Type':'application/json'
            }

        } ).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{
            console.log(resultado2.name)
        })
        .catch(error=>{
            console.error('Hubo un error al ejecutarse',error)
        })


    })

    btngrabar2.addEventListener('click',function(){
        //`https://clases2020-e5713.firebaseio.com/Cursos/${txtCodigo.value}.json`
        let url = `https://proyectoejemplo-11b82.firebaseio.com/Cursos/${txtcodigo.value}.json`
        let cuerpo =
         {
             Codigo: txtcodigo.value ,
             Descripcion: txtdescripcion.value
            
         }

        fetch(url , {
            method:'PUT',
            body:  JSON.stringify(cuerpo) ,
            headers:{
                'Content-Type':'application/json'
            }

        } )
        .then(respuesta=>{
            return respuesta.json()
        })
        .then(respuesta2=>{
            console.log(respuesta2)
        })
        .catch(error=>{
            console.error('No se pudo grabar el nodo curso',error);
        })
    })

    btneliminar.addEventListener('click',function(){
        let url = `https://proyectoejemplo-11b82.firebaseio.com/Cursos/${txtcodigo.value}.json`

        fetch(url , {
            method:'DELETE'
        } )
        .then(resultado=>{
            return resultado.json()
        })
        .then(resultado2=>{
            console.log(resultado2)
        })
        .catch(error=>{
            console.error('No se pudo eliminar el curso',error)
        })


    })

})