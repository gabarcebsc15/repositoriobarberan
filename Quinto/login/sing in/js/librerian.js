const columna = document.getElementById("colum")
const fila = document.getElementById("fil")
const nombre = document.getElementById("name")
const apellido = document.getElementById("ape")
const email = document.getElementById("emaill")
const cel = document.getElementById("celular")
const parrafo = document.getElementById("warnings")

botongenerar.addEventListener("click", e=>{
    e.preventDefault()
    let warnings = ""
    let entrar = false    
    let patron = /^[a-zA-Z\s]*$/; 
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/
    parrafo.innerHTML = ""
    if(columna.value != 1 && columna.value != 2 && columna.value != 3 && columna.value != 4 
     &&columna.value != 5 && columna.value != 6 && columna.value != 7 && columna.value != 8){
        entrar = true
        warnings+="Columna incorrecta </br>"
    }else {
        sessionStorage.setItem("columna", colum.value);
    }//Guardo valores para utilizarlo como posición en el alfil
    
   if(fila.value != 1 && fila.value != 2 && fila.value != 3 && fila.value != 4 
    &&fila.value != 5 && fila.value != 6 && fila.value != 7 && fila.value != 8){
    entrar = true
    warnings+="Fila incorrecta </br>"
   }else {
    sessionStorage.setItem("fila", fil.value);
    }//Guardo valores para utilizarlo como posición en el alfil

   if (nombre.value == ""){
    entrar = true
    warnings+="Campo vacío en nombre </br>"
   }else {
    sessionStorage.setItem("nombre", name.value);
    }

   if(!patron.test(nombre.value)){
    entrar = true
    warnings+="No se admiten números en el nombre</br>"
   }else {
   sessionStorage.setItem("nombre", name.value);
   }

   if (apellido.value == ""){
    entrar = true
    warnings+="Campo vacío en apellido </br>"
   }

   if(!patron.test(apellido.value)){
    entrar = true
    warnings+="No se admiten números en apellido</br>"
   }

    if(!regexEmail.test(email.value)){
        warnings += `El email no es valido <br>`
        entrar = true
    }else{        
     sessionStorage.setItem("email", emaill.value);
    }

    if (celular.value == ""){
        entrar = true
        warnings+="Campo vacío en celular </br>"
    }
    else{
        sessionStorage.setItem("celular", celular.value)
    }
    if(celular.value.length!=10){
        entrar = true
        warnings+="Celular debe contener 10 números </br>"
    }else{
        sessionStorage.setItem("celular", celular.value)
    }
    
    if(entrar){
        parrafo.innerHTML = warnings
    }else{
        parrafo.innerHTML = "Enviado";
          setTimeout (redireccionar(), 20000);
    }

    function redireccionar(){
        location.href="./index.html";
    }
})